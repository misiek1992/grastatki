﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace GRASTATKI.Models
{
    public class MyContext : DbContext
    {
        public MyContext() : base(@"server=localhost;user id=root;password=root;database=statki;persistsecurityinfo=True;") {}
        public DbSet<Player> Players { get; set; }
        public DbSet<Field> Fields { get; set; }
        
    }
}