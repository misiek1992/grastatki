﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GRASTATKI.Models
{
    public enum Plac
    {
        Puste = 0,
        Statek = 1,
        Zatopiony = 2,
        Pudlo = 3
    }

    public class Field
    {
        public int FieldId { get; set; }
        public int PlayerId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public Plac Place { get; set; }
        public virtual Player Player { get; set; }
    }
}