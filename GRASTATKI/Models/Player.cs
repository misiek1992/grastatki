﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GRASTATKI.Models
{
    public class Player
    {
        public int PlayerId { get; set; }
        public string PlayerName { get; set; }
    }
}