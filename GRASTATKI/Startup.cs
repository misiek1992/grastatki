﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GRASTATKI.Startup))]
namespace GRASTATKI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
