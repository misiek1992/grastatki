﻿using GRASTATKI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GRASTATKI.Controllers
{
    public class HomeController : Controller
    {
        Random r = new Random();
        Player FirstPlayer = new Player();
        public string Player1 { get; set; }
        public string Player2 { get; set; }
        MyContext _myContext = new MyContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpGet]
        public ActionResult Players()
        {
            //Player user = new Player();
            //user.PlayerName = Player1 ;
            //_myContext.Players.Add(user);
            //_myContext.SaveChanges();
            //user.PlayerName = Player2;
            //_myContext.Players.Add(user);
            //_myContext.SaveChanges();


            var rnd = new Random(DateTime.Now.Millisecond);
            var randomNumbers = Enumerable.Range(0, 6 * 6)
                 .OrderBy(x => rnd
                .Next())
                .Take(3)
                .ToList();
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (randomNumbers.Contains(j + (i * 6)))
                    {
                        _myContext.Fields.Add(new Field() { X = i, Y = j, Place = Plac.Statek});
                    }
                    else
                    {
                        _myContext.Fields.Add(new Field() { X = i, Y = j, Place = Plac.Puste });
                    }
                }
            }
            return View();
        }
        public ActionResult Losowanie()
        {
            return View();
        }
    }
}